# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Before any major/minor/patch is released all unit tests will be run to verify they pass.

## [Unreleased] - These changes have not been added to RubyGems yet

- [ ]

## [1.1.2] - 2024-06-29

### Added

- kwarg `bits` for overriding the type of GUI that is launched on startup.
  - Defaults to 64, set it to either 32 or 64

### Fixed

- {Remote}.run_voicemeeter now launches x64 bit GUI's for all kinds if Ruby detects a 64 bit system.

## [1.1.0] - 2023-06-28

### Added

- Configurable kwarg `login_timeout`, defaults to 2s.

## [1.0.1] - 2023-09-14

### Added

- BusDevice added to Bus class.

## [1.0.0] - 2023-09-01

### Added

- Bindings and abstraction classes are implemented
- An event system:
  - `pdirty`: parameter updates on the main Voicemeeter GUI
  - `mdirty`: macrobutton updates
  - `midi`: incoming data from a connected midi device
  - `ldirty`: channel level updates
- An entry point for fetching a Remote class for each kind of Voicemeeter (basic, banana, potato)
- Logging system for reading messages sent by getters and setters.
- String class refinement in util.rb. It's scope should be limited to the CBindings module.
- rbs type signatures but some of them need updating.
- example user profile configs included with repo.
